/** Change the Select2 Placeholder text **/
(function() {
  Drupal.behaviors.select2_facet = {
    attach: function (context, settings) {
      $(".select2-search__field").attr('placeholder','- Select -');
    }
  };
})();
