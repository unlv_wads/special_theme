/* --------------------------------------------- 
* Filename:     custom.js
* Version:      1.0.0 (2016-03-05)
* Website:      http://www.zymphonies.com
* Description:  Global Script
* Author:       Zymphonies Team
                info@zymphonies.com
-----------------------------------------------*/

function clients_owl(){
	jQuery('.field--name-field-clients-logo').owlCarousel({
		items: 2,
		margin:10,
		dots: true,
		autoPlay: 3000,
		navigation : true,
		responsive : {
			500:{ items: 2, dots: true, navigation : true },
			700:{ items: 3, dots: true, navigation : true },
			900:{ items: 4, dots: true, navigation : true }
		}
	});
}

function service_owl(){
	jQuery('.field--name-field-service').owlCarousel({
		items: 1,
		margin:10,
		dots: true,
		autoPlay: 3000,
		navigation : true,
		responsive : {
			500:{ items: 1, dots: true, navigation : true },
			700:{ items: 2, dots: true, navigation : true },
			900:{ items: 3, dots: true, navigation : true }
		}
	});
}

wow = new WOW({
	boxClass: 'wow',
	animateClass: 'animated',
	offset: 0,
	mobile: false,
	live: true
});


function theme_menu(){

	//Main menu
	jQuery('#main-menu').smartmenus();
	
	//Mobile menu toggle
	jQuery('.navbar-toggle').click(function(){
		jQuery('.region-primary-menu').slideToggle();
	});

	//Mobile dropdown menu
	if ( jQuery(window).width() < 767) {
		jQuery(".region-primary-menu li a:not(.has-submenu)").click(function () {
			jQuery('.region-primary-menu').hide();
	    });
	}

}

function theme_home(){
	
	//flexslider
	jQuery('.flexslider').flexslider({
    	animation: "slide"	
    });

	// primary-menu 
	jQuery('.region-primary-menu a').click(function() {
      if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
          var target = jQuery(this.hash);
          target = target.length ? target : jQuery('[name=' + this.hash.slice(1) + ']');
          if (target.length) {
              jQuery('html,body').animate({
                  scrollTop: target.offset().top
              }, 1000);
              return false;
          }
      	}
  	});

}

function theme_scroll(){
	jQuery(window).ready(function() {
		jQuery(window).scroll(function (){
			if(jQuery(this).scrollTop()>25){
				jQuery('.topnav').css('background','white');
				jQuery('.topnav').css('height', '60px');
				jQuery('.region-primary-menu').css('height', '60px');
				jQuery('.brand').css('height', '60px');
				jQuery('.brand').find('img').css('height', '60px');
				jQuery('.menu-base-theme').css('padding-top', '0px');
				jQuery('#gotoTop').css('display', 'block');
				jQuery('.headerSearch').css('position','fixed');
				jQuery('.headerSearch').css('left','20%');
				jQuery('.menu-base-theme').find('ul').css('margin-top','3px important');
			}
			if(jQuery(this).scrollTop()>175){
				jQuery('.headerSearch').removeClass('fadeIn');
				jQuery('.headerSearch').addClass('fadeOut');
			}
			if(jQuery(this).scrollTop()>225){
				jQuery('.headerSearch').css('display', 'none');
			}
			if (jQuery(this).scrollTop()<25){
				jQuery('.topnav').css('background-color','transparent');
				jQuery('.topnav').css('height','100px');
				jQuery('.region-primary-menu').css('height','100px');
				jQuery('.brand').css('height','100px');
				jQuery('.brand').find('img').css('height','80px');
				jQuery('.menu-base-theme').css('padding-top', '30px');
				jQuery('#gotoTop').css('display', 'none');
				jQuery('.headerSearch').css('margin-top', '0px');
				jQuery('.headerSearch').css('position','inherit');
				jQuery('.headerSearch').css('left','20%');
				jQuery('.menu-base-theme').find('ul').css('margin-top','13px important');
			}else{
				jQuery('.topnav').css('background','white');
				jQuery('.topnav').css('height', '60px');
				jQuery('.region-primary-menu').css('height', '60px');
				jQuery('.brand').css('height', '60px');
				jQuery('.brand').find('img').css('height', '60px');
				jQuery('.menu-base-theme').css('padding-top', '0px');
				jQuery('#gotoTop').css('display', 'block');
				jQuery('.headerSearch').css('position','fixed');
				jQuery('.headerSearch').css('left','20%');
				jQuery('.menu-base-theme').find('ul').css('margin-top','3px important');
			}
			if (jQuery(this).scrollTop()<175){
				jQuery('.headerSearch').removeClass('fadeOut');
				jQuery('.headerSearch').addClass('fadeIn');
			}else{
				jQuery('.headerSearch').removeClass('fadeIn');
				jQuery('.headerSearch').addClass('fadeOut');
			}
			if(jQuery(this).scrollTop()>225){
				jQuery('.headerSearch').css('display', 'none');
			}else{
				jQuery('.headerSearch').css('display', 'block');
			}
		});
	});
}
function theme_load(){
	jQuery(window).ready(function() {
/*		if (jQuery(this).scrollTop()<25){
			jQuery('.topnav').css('background-color','transparent');
			jQuery('.topnav').css('height','100px');
			jQuery('.region-primary-menu').css('height','100px');
			jQuery('.brand').css('height','100px');
			jQuery('.brand').find('img').css('height','80px');
			jQuery('.menu-base-theme').css('padding-top', '30px');
			jQuery('#gotoTop').css('display', 'none');
		}else{
			jQuery('.topnav').css('background','white');
			jQuery('.topnav').css('height', '60px');
			jQuery('.region-primary-menu').css('height', '60px');
			jQuery('.brand').css('height', '60px');
			jQuery('.brand').find('img').css('height', '60px');
			jQuery('.menu-base-theme').css('padding-top', '0px');
			jQuery('#gotoTop').css('display', 'block');
		}*/
		jQuery('#gotoTop').click(function(){
			jQuery("html, body").animate({ scrollTop: 0 }, 600);
			return false;
		});
		var pTags=null;
		var oTags=null;
		for(i=1;i<40; i++){
			pTags = jQuery( "#snav-content"+i );
			if ( pTags.parent().is( "div" ) ) {pTags.unwrap();}
			if ( pTags.parent().is( "div" ) ) {pTags.unwrap();}
			oTags = jQuery( "#side-navigation" );
			if ( oTags.parent().is( "header" ) ) {oTags.unwrap();}
		}
		$(".views-view-grid").appendTo("#side-navigation");
		jQuery(".customjs").hide().first().show();
		jQuery(".sidenav li:first").addClass("ui-tabs-active");
		jQuery(".sidenav a").on('click',function (e){
			var newID=jQuery(this).attr('id');
			if(newID.trim()=='toExternal'){
				var newURL=jQuery(this).attr('href');
				window.location.href=newURL;
				return true;
			}else{
				e.preventDefault();
				jQuery(this).closest('li').addClass("ui-tabs-active").siblings().removeClass("ui-tabs-active");
				jQuery(jQuery(this).attr('href')).show().siblings('.customjs').hide();
			}
		});
		var hash = jQuery.trim( window.location.hash );
		if (hash) jQuery('.sidenav a[href$="'+hash+'"]').trigger('click');
	});
}

jQuery(document).ready(function($){
		$('.paragraph--type--text-large-image-left .image-style-large-image-home').attr('data-animate','fadeInLeftBig');
		var lloadIMG=$('.paragraph--type--text-large-image-left .image-style-large-image-home').attr('src');
		$('.paragraph--type--text-large-image-left .image-style-large-image-home').attr('data-lazyload',lloadIMG);
	wow.init();
	clients_owl();
	service_owl();
	theme_menu();
	theme_home();
	theme_load();
	//theme_scroll();
});